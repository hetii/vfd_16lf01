#include <util/delay.h>   //delay
#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>

/*
 * This code is based on:
 * https://github.com/DnaX/Samsung_16LF01_VFD/
 *
 */

#define VFD_CMD_BUFFER_POINTER 0b10100000
#define VFD_CMD_DIGIT_COUNTER 0b11000000
#define VFD_CMD_MASK 0b00001111
#define VFD_CMD_BRIGHTNESS 0b11100000
#define VFD_CMD_BRIGHTNESS_MASK 0b00011111

//#define _NOP(amount) do { __asm__ __volatile__ ("nop"); } while (amount)
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)

static uint8_t _digits_count;

//#define ASM_SBC_zero(dest) asm volatile ("sbc %0, __zero_reg__ " : "=r" (dest) :)
//PB0 - RESET
//Pd7 - CLK
//Pd6 - DATA
//

void vfd_16LF01_reset(void){
  PORTB &= ~(1<<PB0);
  _delay_ms(2);
  PORTB |= (1<<PB0);
  _delay_ms(2);
}

void vfd_16LF01_command(uint8_t data){
  static int8_t i=0, x=0;
  for (i=7; i>=0; i--) {
    if (bitRead(data, i)){
       PORTD |= (1<<PD6);
    } else {
       PORTD &= ~(1<<PD6);
    }

    PORTD |= (1<<PD7); //SCK HIGH
    _delay_ms(1);
    //_NOP(2);
    PORTD &= ~(1<<PD7);//SCK LOW
    _delay_ms(1);
    //_NOP(2);
    PORTD |= (1<<PD7); //SCK HIGH
    // Internal processing time (50us)
    //_NOP(50);
    //
    //_delay_ms(1);
    do {
    __asm__ __volatile__ ("nop");
    } while (x--);
  }


}

// Set display brightness, from 0 to 31.
void vfd_16LF01_set_brightness(uint8_t amount) {
  if (amount > 31) {
    amount = 31;
  }
  amount &= VFD_CMD_BRIGHTNESS_MASK;
  vfd_16LF01_command(VFD_CMD_BRIGHTNESS | amount);
}

// Set number of digits to control.
void vfd_16LF01_set_digits_count(uint8_t digits) {
  if (digits > 16) {
    digits = 16;
  }
  
  _digits_count = digits;
  
  digits &= VFD_CMD_MASK;
  vfd_16LF01_command(VFD_CMD_DIGIT_COUNTER | digits);
}

// Set buffer poiner position.
void vfd_16LF01_set_cursor(uint8_t pos) {
  if (pos > 15) {
    pos = 15;
  }
  
  // First digit value is B1111, from second start from 0
  if (pos == 0) {
    pos = 0b1111;
  }
  else {
    pos--;
  }
  
  pos &= VFD_CMD_MASK;
  vfd_16LF01_command(VFD_CMD_BUFFER_POINTER | pos);
}

// Return to position 1
void vfd_16LF01_home() { 
  vfd_16LF01_set_cursor(0);
}

// Write a char on the display
inline uint8_t vfd_16LF01_write(uint8_t _char) {
  // First set of charmap (letters ad some symbols)
  if (_char >= 64 && _char <= 95) {
    vfd_16LF01_command(_char - 64);
  }
  // Second set of charmap (numbers and some symbols)
  else if (_char >= 32 && _char <= 63) {
    vfd_16LF01_command(_char);
  }
  // Lower case letters
  else if (_char >= 97 && _char <= 122) {
    vfd_16LF01_command(_char - 96);
  }
  return 1;
}

// Blank the display by setting up to 16 spaces
void vfd_16LF01_clear() {
  static uint8_t i=0;
  vfd_16LF01_set_cursor(0);
  for (i=0; i<_digits_count; i++) {
    vfd_16LF01_write(' ');
  }
}

void vfd_16LF01_print(char *chp) {
  while (*chp){
     vfd_16LF01_write(*chp++);
  }
}

int main(){
  static uint8_t c;
  DDRD |= (1<<6)|(1<<7);
  DDRB |= (1<<0);

  DDRC |= (1<<5);
 
  vfd_16LF01_reset();
  vfd_16LF01_set_digits_count(16);

  vfd_16LF01_clear();
  vfd_16LF01_set_brightness(31);
  
  vfd_16LF01_print("Ania Bielawiec:)");
  _delay_ms(7000);

  while(1){
    vfd_16LF01_clear();
    for(c = 60; c<=122; c++){
      _delay_ms(150);
      vfd_16LF01_write(c);
      PORTC ^= (1<<5) ;
    }
  }
  return 0;
}

