local M
do
  local PIN_DAT = nil
  local PIN_SCK = nil
  local PIN_RST = nil
  local vfd_16LF01_ready=0
  local function vfd_16LF01_reset()
    gpio.write(PIN_RST,1)
    tmr.delay(2000)
    gpio.write(PIN_RST,0)
    tmr.delay(2000)
  end
  local function vfd_16LF01_command(data)
    for i = 7, 0, -1 do
      if bit.isset(data, i) then
        gpio.write(PIN_DAT,0)
      else
        gpio.write(PIN_DAT,1)
      end
      gpio.write(PIN_SCK,0)
      tmr.delay(5)
      gpio.write(PIN_SCK,1)
      tmr.delay(5)
      gpio.write(PIN_SCK,0)
      tmr.delay(40) 
    end
  end
  local function vfd_16LF01_set_brightness(amount)
    if amount > 31 then
      amount = 31
    end
    amount = bit.band(amount,31)
    vfd_16LF01_command(bit.bor(224,amount))
  end
  local function vfd_16LF01_set_digits_count(digits)
    if digits > 16 then
      digits = 16
    end
    _digits_count = digits
    digits = bit.band(digits, 15)
    vfd_16LF01_command(bit.bor(192,digits))
  end
  local function vfd_16LF01_set_cursor(pos)
    if pos > 15 then
      pos = 15
    end
    if pos == 0 then
      pos = 15
    else
      pos = pos-1
    end
    pos = bit.band(pos,15)
    vfd_16LF01_command(bit.bor(160,pos))
  end
  local function vfd_16LF01_home()
    vfd_16LF01_set_cursor(0)
  end
  local function vfd_16LF01_write(_char)
    --_char = string.byte(_char)
    if (_char >= 64 and _char <= 95) then
      vfd_16LF01_command(_char - 64)
    elseif (_char >= 32 and _char <= 63) then
      vfd_16LF01_command(_char)
    elseif (_char >= 97 and _char <= 122) then
      vfd_16LF01_command(_char - 96)
    end
  end
  local function vfd_16LF01_clear()
    vfd_16LF01_set_cursor(0)
    for i = 0, _digits_count-1, 1 do
      vfd_16LF01_write(32)
    end
  end
  local function vfd_16LF01_init(dat, sck, rst)
    PIN_DAT = dat or 3 --DATA
    PIN_SCK = sck or 8 --CLK
    PIN_RST = rst or 7 --RESET
    gpio.mode(PIN_DAT,1)
    gpio.mode(PIN_RST,1)
    gpio.mode(PIN_SCK,1)
    gpio.write(PIN_DAT,0)
    gpio.write(PIN_RST,0)
    gpio.write(PIN_SCK,0)
    vfd_16LF01_reset()
    vfd_16LF01_set_digits_count(16)
    vfd_16LF01_clear()
    vfd_16LF01_set_brightness(31)
    vfd_16LF01_ready=1
    return M
  end
  local function vfd_16LF01_print(str)
    if vfd_16LF01_ready==0 then
      vfd_16LF01_init()
    end
    vfd_16LF01_set_cursor(0)
    local k = 0
    for i = 1, #str do
      local c = str:sub(i,i)
      if ((c~="\r") and (c~="\n")) then
        vfd_16LF01_write(string.byte(c))
        k=k+1
      end
    end
    while k < _digits_count do
      vfd_16LF01_write(32)
      k=k+1
    end
  end
  M = {
    init = vfd_16LF01_init,
    clear = vfd_16LF01_clear,
    print = vfd_16LF01_print, 
    brightness = vfd_16LF01_set_brightness,
    cmd = vfd_16LF01_command
  }
end
return M
